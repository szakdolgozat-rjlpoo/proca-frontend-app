FROM node:alpine as build

COPY . /workspace/app
WORKDIR /workspace/app
RUN npm install; npm run build; ls dist/frontend-app

FROM nginx:stable-alpine

EXPOSE 80

COPY --from=build /workspace/app/dist/frontend-app /usr/share/nginx/html
