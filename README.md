# FrontendApp

[![pipeline status](https://gitlab.com/szakdolgozat-rjlpoo/proca-frontend-app/badges/master/pipeline.svg)](https://gitlab.com/szakdolgozat-rjlpoo/proca-frontend-app/commits/master)
[![coverage report](https://gitlab.com/szakdolgozat-rjlpoo/proca-frontend-app/badges/master/coverage.svg)](https://gitlab.com/szakdolgozat-rjlpoo/proca-frontend-app/commits/master)

- [Angular](#angular)
  - [Development server](#development-server)
  - [Code scaffolding](#code-scaffolding)
  - [Build](#build)
  - [Running unit tests](#running-unit-tests)
  - [Further help](#further-help)
- [Serve application](#serve-application)
  - [Docker](#docker)
  - [Nginx](#nginx)

## Angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.6.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Serve application

### Docker

Application is containerized which makes it easier to deploy the application as is.

The container relies on Nginx which has to be configured according to the environment. An example configuration can be found in the [Nginx section](#nginx).

### Nginx

In production we recommend to [build](#build) the application and serve the static files. In this case angular will not take of WebSocket connections which means Nginx has to be configured accordingly.

The recommended nginx configuration is the following:

```
events { }

http {
    map $http_upgrade $connection_upgrade {
        default upgrade;
        '' close;
    }

    server {
        include /etc/nginx/mime.types;

        listen 80;

        location /ws {
            proxy_pass http://shop-gateway:8080/ws;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $connection_upgrade;
        }

        location / {
            root /usr/share/nginx/html;
            try_files $uri $uri/ /index.html;
        }
    }
}
```
