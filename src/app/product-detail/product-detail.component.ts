import { Component, OnDestroy, OnInit } from '@angular/core';
import { WebSocketService } from '../service/web-socket.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Product } from '../domain/product.domain';
import { Time } from '../domain/time.enum';
import { ProductService } from '../service/product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
})
export class ProductDetailComponent implements OnInit, OnDestroy {

  private productDetailsSubscription: Subscription;
  private priceChangeSubscription: Subscription;
  private paramsSubscription: Subscription;
  private reconnectTimeout: any;

  private id: number;
  private product: Product;
  private loading = true;
  private connected = false;

  constructor(
    private productService: ProductService,
    private webSocketService: WebSocketService,
    private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.paramsSubscription = this.activatedRoute.params.subscribe(value => {
      this.id = Number(value.id);

      if (!this.connected) {
        this.connect();
      }
    });
  }

  private reconnect(): void {
    this.loading = true;
    this.connected = false;

    this.reconnectTimeout = setTimeout(() => {
      this.connect();
    }, 10 * Time.SECOND);
  }

  private connect(): void {
    this.loading = true;
    this.connected = false;
    this.reconnectTimeout = null;

    this.priceChangeSubscription = this.webSocketService.priceChangeSubscribe({
      next: priceChange => {
        this.productService.updatePrices(this.product, priceChange);
      }
    });

    this.productDetailsSubscription = this.webSocketService.productDetailsSubscribe(
      this.id,
      {
        next: value => {
          this.loading = false;
          this.connected = true;
          this.product = value;
        },
        error: err => {
          const uncleanClose = err instanceof CloseEvent && !err.wasClean;
          if (err instanceof Event || uncleanClose) {
            this.reconnect();
          }
        },
      });
  }

  ngOnDestroy(): void {
    clearTimeout(this.reconnectTimeout);

    if (this.priceChangeSubscription != null) {
      this.priceChangeSubscription.unsubscribe();
      this.priceChangeSubscription = null;
    }

    if (this.productDetailsSubscription != null) {
      this.productDetailsSubscription.unsubscribe();
      this.productDetailsSubscription = null;
    }

    if (this.paramsSubscription !== null) {
      this.paramsSubscription.unsubscribe();
      this.paramsSubscription = null;
    }
  }

}
