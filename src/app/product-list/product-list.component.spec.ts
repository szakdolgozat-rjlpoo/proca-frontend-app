import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductListComponent } from './product-list.component';
import { ProductListItemComponent } from '../product-list-item/product-list-item.component';
import { MatCardModule, MatPaginatorModule, MatProgressBarModule } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

describe('ProductListComponent', () => {
  let component: ProductListComponent;
  let fixture: ComponentFixture<ProductListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProductListComponent,
        ProductListItemComponent,
      ],
      imports: [
        MatCardModule,
        MatPaginatorModule,
        MatProgressBarModule,
        RouterTestingModule,
      ],
      providers: [
        {provide: ActivatedRoute, useValue: {queryParams: of({})}},
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
