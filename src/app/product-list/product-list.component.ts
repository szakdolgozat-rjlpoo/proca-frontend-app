import { Component, OnDestroy, OnInit } from '@angular/core';
import { WebSocketService } from '../service/web-socket.service';
import { Subscription } from 'rxjs';
import { Page } from '../domain/page.domain';
import { Product } from '../domain/product.domain';
import { PageEvent } from '@angular/material';
import { Time } from '../domain/time.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { PageRequest } from '../domain/page-request.domain';
import { ProductService } from '../service/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
})
export class ProductListComponent implements OnInit, OnDestroy {

  private static readonly DEFAULT_PAGE_SIZE = 8;

  private productListSubscription: Subscription;
  private priceChangeSubscription: Subscription;
  private queryParamsSubscription: Subscription;
  private reconnectTimeout: any;

  private page: Page<Product>;
  private pageRequest: PageRequest;
  private loading = true;
  private connected = false;

  constructor(
    private productService: ProductService,
    private webSocketService: WebSocketService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.queryParamsSubscription = this.activatedRoute.queryParams.subscribe(params => {
      this.pageRequest = {
        page: params.page || 0,
        size: params.size || ProductListComponent.DEFAULT_PAGE_SIZE,
      };

      if (this.connected) {
        this.next();
      } else {
        this.connect();
      }
    });
  }

  private next(): void {
    this.loading = true;

    this.webSocketService.nextProductList(this.pageRequest);
  }

  private reconnect(): void {
    this.loading = true;
    this.connected = false;

    this.reconnectTimeout = setTimeout(() => {
      this.connect();
    }, 10 * Time.SECOND);
  }

  private connect(): void {
    this.loading = true;
    this.connected = false;
    this.reconnectTimeout = null;

    this.priceChangeSubscription = this.webSocketService.priceChangeSubscribe({
      next: priceChange => {
        for (const product of this.page.content) {
          if (product.id !== priceChange.productId) {
            continue;
          }
          this.productService.updatePrices(product, priceChange);
        }
      }
    });

    this.productListSubscription = this.webSocketService.productListSubscribe(
      this.pageRequest,
      {
        next: value => {
          this.loading = false;
          this.connected = true;
          this.page = value;
        },
        error: err => {
          const uncleanClose = err instanceof CloseEvent && !err.wasClean;
          if (err instanceof Event || uncleanClose) {
            this.reconnect();
          }
        },
      });
  }

  ngOnDestroy(): void {
    clearTimeout(this.reconnectTimeout);

    if (this.priceChangeSubscription != null) {
      this.priceChangeSubscription.unsubscribe();
      this.priceChangeSubscription = null;
    }

    if (this.productListSubscription != null) {
      this.productListSubscription.unsubscribe();
      this.productListSubscription = null;
    }

    if (this.queryParamsSubscription != null) {
      this.queryParamsSubscription.unsubscribe();
      this.queryParamsSubscription = null;
    }
  }

  onPageEvent($event: PageEvent): void {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: {page: $event.pageIndex, size: $event.pageSize},
    });
  }
}
