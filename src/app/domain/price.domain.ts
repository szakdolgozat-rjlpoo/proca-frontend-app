import {PriceDetail} from './price-detail.domain';

export interface Price {
  id: number;
  currencyCode: string;
  value: number;
  detail: PriceDetail;
  updatedAt: string;
}
