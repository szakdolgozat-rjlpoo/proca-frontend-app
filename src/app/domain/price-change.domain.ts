import {Price} from './price.domain';

export interface PriceChange {
  productId: number;
  price: Price;
}
