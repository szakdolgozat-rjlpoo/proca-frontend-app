import { Price } from './price.domain';

export interface Product {
  id: number;
  name: string;
  description?: string;
  img?: string;
  prices: Price[];
  categories: string[];
}
