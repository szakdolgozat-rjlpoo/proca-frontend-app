export interface PriceDetail {
  supplier?: string;
  stock: number;
}
