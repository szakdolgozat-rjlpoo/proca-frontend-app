import { Sort } from './sort.domain';

export interface Page<T> {
  content: Array<T>;
  totalElements: number;
  number: number;
  size: number;
  sort: Sort[];
}
