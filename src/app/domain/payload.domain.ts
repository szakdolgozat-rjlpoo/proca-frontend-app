export interface Payload<T> {
  action: string,
  data?: T,
}
