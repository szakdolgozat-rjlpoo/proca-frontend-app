import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../domain/product.domain';
import { Price } from '../domain/price.domain';

@Component({
  selector: 'app-product-list-item',
  templateUrl: './product-list-item.component.html',
})
export class ProductListItemComponent implements OnInit {

  @Input()
  private item: Product;

  private get cheapestPrice(): Price {
    if (!this.item || !this.item.prices || !this.item.prices.length) {
      return null;
    }
    let prices = this.item.prices.filter(value => value.detail.stock > 0);
    if (!prices.length) {
      return null;
    }
    return prices.reduce((acc, loc) => acc.value < loc.value ? acc : loc);
  }

  constructor() {
  }

  ngOnInit(): void {
  }

}
