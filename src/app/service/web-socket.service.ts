import { Injectable } from '@angular/core';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { Subscription } from 'rxjs';
import { PartialObserver } from 'rxjs/src/internal/types';
import { Page } from '../domain/page.domain';
import { Product } from '../domain/product.domain';
import { PageRequest } from '../domain/page-request.domain';
import { filter, map } from 'rxjs/operators';
import { Time } from '../domain/time.enum';
import { PriceChange } from '../domain/price-change.domain';
import { Payload } from '../domain/payload.domain';

@Injectable({
  providedIn: 'root',
})
export class WebSocketService {

  private reconnectTimeout: any;

  private connected: boolean;
  private subject: WebSocketSubject<Payload<any>>;

  constructor() {
    this.subject = webSocket(WebSocketService.buildWebSocketUrl());

    this.connected = false;
    this.keepAlive();
  }

  private static buildWebSocketUrl(): string {
    const secure = window.location.protocol === 'https:';
    const protocol = secure ? 'wss:' : 'ws:';
    const host = window.location.host;
    return `${protocol}//${host}/ws`;
  }

  private reconnect(): void {
    this.reconnectTimeout = setTimeout(() => {
      this.keepAlive();
    }, 10 * Time.SECOND);
  }

  private keepAlive(): void {
    if (this.connected) {
      return;
    }

    clearTimeout(this.reconnectTimeout);
    this.connected = true;
    this.subject.pipe(filter(message => message.action === 'internal:ping'))
      .subscribe({
        next: value => {
          this.subject.next({action: 'internal:pong'});
        },
        error: err => {
          this.connected = false;
          const uncleanClose = err instanceof CloseEvent && !err.wasClean;
          if (err instanceof Event || uncleanClose) {
            this.reconnect();
          }
        },
        complete: () => {
          this.connected = false;
        }
      });
  }

  public productListSubscribe(pageable: PageRequest, observer?: PartialObserver<Page<Product>>): Subscription {
    this.keepAlive();

    return this.subject
      .multiplex(
        () => {
          return {action: 'product:list', data: {pageable}};
        },
        () => {
          return {action: 'product:unsubscribe'};
        },
        message => message.action === 'product:list',
      )
      .pipe(map(wrapper => wrapper.data))
      .subscribe(observer);
  }

  public productDetailsSubscribe(id: number, observer?: PartialObserver<Product>): Subscription {
    this.keepAlive();

    return this.subject
      .multiplex(
        () => {
          return {action: 'product:details', data: {id}};
        },
        () => {
          return {action: 'product:unsubscribe'};
        },
        message => message.action === 'product:details',
      )
      .pipe(map(wrapper => wrapper.data))
      .subscribe(observer);
  }

  public priceChangeSubscribe(observer?: PartialObserver<PriceChange>): Subscription {
    this.keepAlive();

    return this.subject
      .pipe(filter(message => message.action === 'price:change'), map(wrapper => wrapper.data))
      .subscribe(observer);
  }

  public nextProductList(pageable: PageRequest): void {
    this.subject.next({action: 'product:list', data: {pageable}});
  }

}
