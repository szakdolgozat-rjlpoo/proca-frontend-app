import { Injectable } from '@angular/core';
import { Product } from '../domain/product.domain';
import { PriceChange } from '../domain/price-change.domain';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor() {
  }

  public updatePrices(product: Product, priceChange: PriceChange) {
    const newPrice = priceChange.price;
    const prices = product.prices;
    for (let j = 0; j < prices.length; j++) {
      const p = prices[j];
      if (p.id === newPrice.id) {
        prices[j] = newPrice;
        return;
      }
    }
    prices.push(newPrice);
  }
}
